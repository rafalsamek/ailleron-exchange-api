<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Reliese\Database\Eloquent\Model;

/**
 * Class ExchangeRate
 * 
 * @property int $id
 * @property Carbon $date
 * @property int $base_currency_id
 * @property int $dest_currency_id
 * @property float $exchange_rate
 * 
 * @property Currency $currency
 *
 * @package App\Models
 */
class ExchangeRate extends Model
{
	protected $table = 'exchange_rates';
	public $timestamps = false;

	protected $casts = [
		'base_currency_id' => 'int',
		'dest_currency_id' => 'int',
		'exchange_rate' => 'float'
	];

	protected $dates = [
		'date'
	];

	protected $fillable = [
		'date',
		'base_currency_id',
		'dest_currency_id',
		'exchange_rate'
	];

	public function currency()
	{
		return $this->belongsTo(Currency::class, 'dest_currency_id');
	}
}
