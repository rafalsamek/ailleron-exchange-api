<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Reliese\Database\Eloquent\Model;

/**
 * Class Currency
 * 
 * @property int $id
 * @property string $code
 * 
 * @property Collection|ExchangeRate[] $exchange_rates
 *
 * @package App\Models
 */
class Currency extends Model
{
	protected $table = 'currencies';
	public $timestamps = false;

	protected $fillable = [
		'code'
	];

	public function exchange_rates()
	{
		return $this->hasMany(ExchangeRate::class, 'dest_currency_id');
	}
}
