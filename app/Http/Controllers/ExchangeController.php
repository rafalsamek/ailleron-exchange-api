<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB as DB;


class ExchangeController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param string $baseCurrency
     * @param string $destCurrency
     * @param float $exchangeRate
     *
     * @return JsonResponse
     */
    public function exchange(string $baseCurrency, string $destCurrency, float $exchangeRate):JsonResponse
    {
        $rate = DB::table('exchange_rates AS e')
            ->leftJoin('currencies AS cbase', 'e.base_currency_id', '=', 'cbase.id')
            ->leftJoin('currencies AS cdest', 'e.dest_currency_id', '=', 'cdest.id')
            ->select('cbase.code AS base', 'cdest.code AS destination', 'e.exchange_rate', 'e.date')
            ->where('cbase.code', '=', "$baseCurrency")
            ->where('cdest.code', '=', "$destCurrency")
            ->orderByDesc('e.date')
            ->limit(1)
            ->first();

        if(empty($rate)) {
            return response()->json([
                'data' => 'No rates found'
            ], 404);
        }

        return response()->json(['data' =>
            [
                'base_value' => $exchangeRate,
                'base_currency' => $baseCurrency,
                'result_value' => $exchangeRate / $rate->exchange_rate,
                'result_currency' => $destCurrency,
            ]
        ]);
    }

}
