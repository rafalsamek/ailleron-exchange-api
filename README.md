Zadanie polega na napisaniu prostego kalkulatora walutowego przy użyciu Laravela. 
Wystarczy nam jeden endpoint, w którym użytkownik na wejściu podaje następujące parametry:

`base_currency` - kod (np. GBP) waluty, z której ma nastąpić przeliczenie

`dest_currency` - kod (np. PLN) waluty, na którą ma nastąpić przeliczenie

`exchange_rate` - kwota do przewalutowania

W odpowiedzi powinniśmy dostać przeliczoną wartość (wedle najświeższego kursu dostępnego w bazie danych) w formacie zgodnym z JSONAPI. W załączeniu przesyłam [strukturę bazy danch](./exchange_rates.sql) do tego zadania.

